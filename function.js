
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var crypto = require("crypto");


var isObjectEmpty = function(obj){
    for(var x in obj){
        if(obj.hasOwnProperty(x)){
            return false;
        }
    }
    return true;
}

var buildRequestUrl = function(requestUri,params){
    if(!isObjectEmpty(params)){
        requestUri += "?";
        for(var x in params){
            if(params.hasOwnProperty(x)){
    
                if(requestUri[requestUri.length-1] !== "?")
                    requestUri += "&";
                
                requestUri += (x+"="+params[x]);
            }
        }
    }
    return requestUri;
}

var getCosmosMetrics = function(context,cosmosDBInfo,params,headers,callback){
    
    var requestUri = "https://management.azure.com/subscriptions/"+cosmosDBInfo.subscription+
           "/resourceGroups/"+cosmosDBInfo.resourceGroup+
           "/providers/Microsoft.DocumentDB/databaseAccounts/"+cosmosDBInfo.cosmosDBAccount+
           "/metrics";
    
    //append parameters to url
    if(!isObjectEmpty(params)){
        requestUri += "?";
        for(var x in params){
            if(params.hasOwnProperty(x)){
                if(requestUri[requestUri.length-1] !== "?")
                    requestUri += "&";
                
                requestUri += (x+"="+params[x]);
            }
        }
    }
    
    var xhttp = new XMLHttpRequest();
    
    xhttp.addEventListener("readystatechange", function () {
      if (this.readyState === 4 && this.status === 200) {
        if(xhttp.responseText.includes("metricValues")){
          var res = JSON.parse(xhttp.responseText);
          callback(res);
        }else{
            context.log("No Metric Values.. ");
            context.done();
        }
      }else if(this.readyState === 4){
        context.log("getCosmosMetrics Request error Status Code:- "+this.status);
        context.done();
      }
    });
    xhttp.open("GET",requestUri,true);
    xhttp.timeout = 2000;
    xhttp.ontimeout = function () { 
        context.log("Timed out!!!");
        context.done();
    }

    //append headers to request
    for(var x in headers){
        if(headers.hasOwnProperty(x)){
            xhttp.setRequestHeader(x,headers[x]);
        }
    }
    
    xhttp.send();
}

var subtractMinutesFromDate  = function(date,minutes){
    var modifiedDate = new Date(date);
    modifiedDate.setMinutes(date.getMinutes() - minutes);
    return modifiedDate;
}

var appendZeroIfSingleDigit = function(num){
    num += "";
    return (num.length == 1)?("0"+num):num;
}

var getUTCDateAsString = function(d){
    var year = d.getUTCFullYear()+"";   
    var month = appendZeroIfSingleDigit(d.getUTCMonth()+1);
    var day = appendZeroIfSingleDigit(d.getUTCDate());
    
    var hours = appendZeroIfSingleDigit(d.getUTCHours());
    var minutes = appendZeroIfSingleDigit(d.getUTCMinutes());
    var seconds = appendZeroIfSingleDigit(d.getUTCSeconds());
    
    var milliSeconds = d.getUTCMilliseconds();
    
    return [year,month,day].join("-")+"T"+[hours,minutes,seconds].join(":")+"."+milliSeconds+"Z";
}

var getDateAsString = function(d){
    
    var year = d.getFullYear()+"";  
    var month = appendZeroIfSingleDigit(d.getMonth()+1);
    var day = appendZeroIfSingleDigit(d.getDate());
    
    var hours = appendZeroIfSingleDigit(d.getHours());
    var minutes = appendZeroIfSingleDigit(d.getMinutes());
    var seconds = appendZeroIfSingleDigit(d.getSeconds());
    
    var milliSeconds = d.getMilliseconds();
    
    return [year,month,day].join("-")+"T"+[hours,minutes,seconds].join(":")+"."+milliSeconds+"Z";
}

var getAuthorizationToken = function(verb,resourceType,resourceId,date,masterKey){
    var key = new Buffer(masterKey,"base64");
    var text = (verb || "").toLowerCase() + "\n" +   
               (resourceType || "").toLowerCase() + "\n" +   
               (resourceId || "") + "\n" +   
               date.toLowerCase() + "\n" +   
               "" + "\n";
    var body = new Buffer(text,"utf-8");
    var signature = crypto.createHmac("sha256", key).update(body).digest("base64");  
    var MasterToken = "master";  
    var TokenVersion = "1.0";  
    return encodeURIComponent("type=" + MasterToken + "&ver=" + TokenVersion + "&sig=" + signature); 
}

var getCollectionInfo = function(context,maxRetry,databaseAccount, databaseName,collectionName,masterKey,callback){
    var verb = "GET";
    var resourceType = "colls";
    var resourceId = "dbs/"+databaseName+"/colls/"+collectionName;
    var currentDateStr = new Date().toUTCString();
    var headers = {
        "Authorization" : getAuthorizationToken(verb,resourceType,resourceId,currentDateStr,masterKey),
        "Content-Type" : "application/json",
        "x-ms-version" : "2017-02-22",
        "x-ms-date": new Date().toUTCString()
    };
    
    var requestUri = "https://"+databaseAccount+".documents.azure.com/dbs/"+databaseName+"/colls/"+collectionName;
    
    var xhttp = new XMLHttpRequest();

    xhttp.addEventListener("readystatechange", function () {
      if (this.readyState === 4 && this.status === 200) {
          var res = JSON.parse(xhttp.responseText);
          callback(res);
      }else if(this.readyState === 4 && this.status === 429){
        if(maxRetry > 0){
            context.log("getCollectionInfo Status Code 429 Retrying...");
            maxRetry = maxRetry - 1;
            getCollectionInfo(context,maxRetry,databaseAccount, databaseName,collectionName,masterKey,callback);
        }else{
            context.log("getCollectionInfo Status 429 Retried multiple times... Exiting");
            context.done();
        }
      }else if(this.readyState === 4){
        context.log("getCollectionInfo Request error Status Code:- "+this.status);
        context.done();
      }
    });

    xhttp.open("GET",requestUri,true);
    xhttp.timeout = 2000;
    xhttp.ontimeout = function () { 
        context.log("Timed out!!!");
        context.done();
     }

    for(var x in headers){
        if(headers.hasOwnProperty(x)){
            xhttp.setRequestHeader(x,headers[x]);
        }
    }
    xhttp.send();
}

var queryOfferByCollection=function(cosmosDBAccount, _self, masterKey,maxRetry,callback){
    var verb = "POST";
    var resourceType = "offers";
    var resourceId = "";
    var currentDateStr = new Date().toUTCString();
    var headers = {
        "Authorization" : getAuthorizationToken(verb,resourceType,resourceId,currentDateStr,masterKey),
        "Content-Type" : "application/query+json",
        "x-ms-version" : "2017-02-22",
        "x-ms-date": currentDateStr,
        "x-ms-documentdb-isquery":"True"
    };
    var requestUri = "https://"+cosmosDBAccount+".documents.azure.com/offers";
    var xhttp = new XMLHttpRequest();

    xhttp.addEventListener("readystatechange", function () {
      if (this.readyState === 4 && this.status === 200) {
          var res = JSON.parse(xhttp.responseText);
          retry=false;
          callback(res);
      }else if(this.readyState === 4 && this.status === 429){
          if(maxRetry > 0){
            maxRetry = maxRetry -1;
            context.log("queryOfferByCollection Status Code 429 Retrying...");
            queryOfferByCollection(cosmosDBAccount, _self, masterKey,maxRetry,callback);
          }else{
            context.log("queryOfferByCollection Status Code 429 Retried multiple times... Exiting");
            context.done();
          }
      }else if(this.readyState === 4){
        context.log("queryOfferByCollection Request error Status Code:- "+typeof(this.status));
        context.done();
      }
    });

    xhttp.open("POST",requestUri,true);
    xhttp.timeout = 2000;
    xhttp.ontimeout = function () {
     context.log("Timed out!!!");
     context.done();
    }
    for(var x in headers){
        if(headers.hasOwnProperty(x)){
            xhttp.setRequestHeader(x,headers[x]);
        }
    }
    var query = {"query":"SELECT * FROM r WHERE (r.resource = '"+_self+"') "};
    xhttp.send(JSON.stringify(query));
}

var getCosmosMetricsFilter = function(metricName,timeGrain){
    var endDate = new Date();
    var startDate = subtractMinutesFromDate(endDate,10);
    
    var startTime = getUTCDateAsString(startDate); //"2018-04-09T09:20:32.4250000Z";
    var endTime = getUTCDateAsString(endDate); //"2018-04-09T09:30:32.425Z";
    
    return "(name.value eq '"+metricName+"') and timeGrain eq duration'"+timeGrain+"' and startTime eq "+startTime+" and endTime eq "+endTime;
}

var updateThroughput = function(cosmosDBAccount,offer,masterKey,callback){
    
    var verb = "PUT";
    var resourceType = "offers";
    var resourceId = offer._rid.toLowerCase();
    var currentDateStr = new Date().toUTCString();
    var authToken = getAuthorizationToken(verb,resourceType,resourceId,currentDateStr,masterKey);
    var headers = {
        "Authorization" : authToken,
        "Content-Type" : "application/json",
        "x-ms-version" : "2017-02-22",
        "x-ms-date": currentDateStr
    };
    var requestUri = "https://"+cosmosDBAccount+".documents.azure.com/offers/"+offer._rid;
    
    var xhttp = new XMLHttpRequest();

    xhttp.addEventListener("readystatechange", function () {
      if (this.readyState === 4 && this.status === 200) {
          var res = JSON.parse(xhttp.responseText);
          callback(res);
      }else if(this.readyState === 4){
        context.log("updateThroughput Request error Status Code:- "+this.status);
        context.done();
      }
    });

    xhttp.open(verb,requestUri,true);
    xhttp.timeout = 2000;
    xhttp.ontimeout = function () { 
        context.log("Timed out!!!");
        context.done();
    }

    for(var x in headers){
        if(headers.hasOwnProperty(x)){
            xhttp.setRequestHeader(x,headers[x]);
        }
    }
    
    xhttp.send(JSON.stringify(offer));
}

var calculateRuChange = function(context,offerResult,metricValues,cosmosInfo,ruUpperBound,ruLowerBound){
    var offer = offerResult["Offers"][0];
    var ruConsumptionPercent = (metricValues[2]/offer.content.offerThroughput)*100;
    context.log("ruConsumptionMedian : "+metricValues[2]+" total : "+offer.content.offerThroughput+" Consumption Percent : "+ruConsumptionPercent);
    if(ruConsumptionPercent >= ruUpperBound){
        if(offer.content.offerThroughput == cosmosInfo.maxRUs){
            context.log("no change in RUs");
            context.done();
            return;
        }
        context.log("doubling RUs");
        offer.content.offerThroughput *= 2;
        if(offer.content.offerThroughput > cosmosInfo.maxRUs) 
            offer.content.offerThroughput = cosmosInfo.maxRUs;
           updateThroughput(cosmosInfo.cosmosDBAccount,offer,cosmosInfo.masterKey,function(res){
                context.log("done"); 
                context.done();
            });
        
    }else if(ruConsumptionPercent <= ruLowerBound){
        if(offer.content.offerThroughput == cosmosInfo.minRUs){
            context.log("no change in RUs");
            context.done();
            return;
        }
        context.log("Reducing RUs by half");
        offer.content.offerThroughput /= 2;
        if(offer.content.offerThroughput < cosmosInfo.minRUs)
            offer.content.offerThroughput = cosmosInfo.minRUs;
        updateThroughput(cosmosInfo.cosmosDBAccount,offer,cosmosInfo.masterKey,function(res){
            context.log("done");   
            context.done();
        });
    }else{
        context.log("no change in RUs");
        context.done();
    }
}

var monitorCosmos = function(context,cosmosInfo,metricsInfo,collectionSelf,ruUpperBound,ruLowerBound){
    metricsInfo.params["$filter"] = getCosmosMetricsFilter(metricsInfo.metricName,metricsInfo.timeGrain);
    getCosmosMetrics(context,cosmosInfo,metricsInfo.params,metricsInfo.headers,
        function(cosmosMetrics){
            cosmosMetrics = cosmosMetrics.value[0].metricValues;
            var metricValues = [];
            var index = cosmosMetrics.length-1;
            for(var i=0;i<5;i++){
                metricValues[i] = cosmosMetrics[index--].total/60;
            }
            metricValues.sort();
            queryOfferByCollection(cosmosInfo.cosmosDBAccount , collectionSelf, cosmosInfo.masterKey,3,function(offerResult){
                calculateRuChange(context,offerResult,metricValues,cosmosInfo,ruUpperBound,ruLowerBound);
            });
        });
    
}
var getToken = function(context,client_id,client_secret,callback){
    var data = "grant_type=client_credentials&resource=https://management.azure.com/&client_id="+client_id+"&client_secret="+client_secret;
    var xhttp = new XMLHttpRequest();
    xhttp.withCredentials = true;

    xhttp.addEventListener("readystatechange", function () {
      if (this.readyState === 4 && this.status === 200) {
          var token = JSON.parse(xhttp.responseText).access_token;
          callback(token);
      }else if(this.readyState === 4){
        context.log("getToken() Request error Status Code:- "+this.status);
        context.done();
      }
    });

    xhttp.open("POST", "https://login.microsoftonline.com/"+process.env["tenent_id"]+"/oauth2/token",true);
    xhttp.timeout = 2000;
    xhttp.ontimeout = function () { 
        context.log("Timed out!!!"); 
        context.done();
    }
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhttp.send(data);
}

var init = function(context){

    var intervalInMinutes = 1;
    var ruUpperBound = 70;
    var ruLowerBound = 30;

    var cosmosInfo = {
        subscription : process.env["subscription_id"],
        resourceGroup : process.env["resourceGroup"],
        cosmosDBAccount : process.env["cosmosDBAccount"],
        masterKey : process.env["masterKey"],
        dbs : process.env["database_name"],
        colls : process.env["collection_name"],
        minRUs:400,
        maxRUs:10000
    };
    var metricsInfo = {
        apiVersion : "2015-04-08",
        metricName : "Total Request Units",
        timeGrain : "PT1M"
    };
    
    metricsInfo.params = {
        "api-version" : metricsInfo.apiVersion,
        "$filter" : ""
    };  
   
    var token = getToken(context,
        process.env["client_id"],
        process.env["client_secret"],
        function(token){
            metricsInfo.headers = {
                "Content-Type" : "application/json",
                "Authorization" : 'Bearer '+token
            };
            getCollectionInfo(context,3,cosmosInfo.cosmosDBAccount,cosmosInfo.dbs,cosmosInfo.colls,cosmosInfo.masterKey,
                function(collectionDoc){
                    monitorCosmos(context, cosmosInfo, metricsInfo, collectionDoc._self, ruUpperBound, ruLowerBound);
                });
        });
}

module.exports = function (context, myTimer) { 
    init(context);
};
#CosmosDB AutoScaling Azure Function

##Overview

This codebase provides an Azure Function in javascript to monitor RUs consumed per sec and scale up or down the RUs.
This script can be scheduled via Azure Time Trigger Functions.


The `function.js` file contains the code which will be triggered periodically by the Function app. The entry point of the code is the 
anonymous exported function which provides the context and the timer parameter which then calls the init function by passing the context.
Inside the init() function the variables `ruUpperBound` and `ruLowerBound` are the upper and lower bound percentage which is by default set
to 70 and 30 respectively, if the collections RU utilization is less then 30 percent then the function is going to reduce the provisioned RU's and increase 
if the RU utilization is more than 70 percent, Similarly the values for `minRUs` and `maxRUs` can be changed which represents the minimum RU's and Maximum RU's 
to which the function is allowed to provision. Inside the `monitorCosmos()` function the `getCosmosMetrics()` function is used to get the CosmosDB collection metrics from the REST endpoint provided by Azure, 
These metrics are used to get the last 5 min RU utilization and divided by 60 and sorted to get sorted array of per second RU utilization for last 5 min. Then the `queryOfferByCollection()` function 
is called which makes a REST call to `https://"+cosmosDBAccount+".documents.azure.com/offers` to get the current provisioned RU's which calls the `calculateRuChange()` method by passing the sorted array of RU utilization.
The `calculateRuChange()` function takes the median value and calculates the current RU utilization percentage which is then used to increase to double or reduce to half if the percentage is more or less than the
upper and lower bound respectively.

```
NOTE: The logic for RU usage calculatation and provesining can by changed if required.
```



##Contents
The project contains three files.
1. function.js (contains the javascript code for the function app)
2. package.json
3. package-lock.json

The `package.json` and `package-lock.json` are the npm dependency files which should be used to install all the
required node dependencies for the `function.js` script.

##Steps for setup

- First create one Azure Function App and then create one time triggered javascript function inside it with 5-10 min interval.
- Once the function is created Go to `https://<function_app_name>.scm.azurewebsites.net`.
- Then click <b>Debug Console > CMD
- Next inside the console go to `D:\home\site\wwwroot` and then drag your `package.json` and `package-lock.json` files to the <b> wwwroot</b> folder at the top half of the page.
- After the files are uploaded, run the npm install command in the Kudu remote execution console.
- Next create one service principal in Azure AD and give access to the CosmosDB account and store the `Application ID` and create one `secret` and copy it.
For more detailed information visit `https://docs.microsoft.com/en-us/azure/azure-resource-manager/resource-group-create-service-principal-portal#create-an-azure-active-directory-application`
- Next go to application settings and add all the following environment variables with the key names listed below.
    1. <b>subscription_id</b>          :The Azure subscription id.
    2. <b>resourceGroup</b>            :The ResourceGroup name which contains the cosmosDB account.
    3. <b>cosmosDBAccount</b>          :CosmosDB account name.
    4. <b>masterKey</b>                :CosmosDB master Key.
    5. <b>database_name</b>            :The name of the CosmosDB database.
    6. <b>collection_name</b>          :Name of the collection which has to be auto scaled.
    7. <b>client_id</b>                :Service Principal application id.
    8. <b>client_secret</b>            :Service Principal secret.
- Next in settings add the following timeout in the host.json file and save.
```json
{
  "functionTimeout": "00:01:00"
}
```
- Next open the Azure Function editor on the console and then copy the code from the `function.js` file and paste it in the editor.
- Once the code is pasted then just click the `save and run` button at the top.